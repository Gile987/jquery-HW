[
    {
        "album": "Where Is Soda Pop And Apple Sweets?",
        "year": "2008",
        "quantity": 2,
        "cover": "img/where.jpg"
    },
    {
        "album": "Tomboyish Love For Daughter",
        "year": "2008",
        "quantity": 5,
        "cover": "img/tomboyish.jpg"
    },
    {
        "album": "My Love Feels All Wrong",
        "year": "2009",
        "quantity": 0,
        "cover": "img/mylove.jpg"
    },
    {
        "album": "Mainstream Muthafucka!",
        "year": "2010",
        "quantity": 3,
        "cover": "img/mainstream.jpg"
    },
    {
        "album": "Steppa",
        "year": "2010",
        "quantity": 7,
        "cover": "img/steppa.jpg"
    },
    {
        "album": "Dancefloor Degrader",
        "year": "2010",
        "quantity": 12,
        "cover": "img/dancefloor.jpg"
    },
    {
        "album": "Overspecialized",
        "year": "2011",
        "quantity": 2,
        "cover": "img/oversp.jpg"
    },
    {
        "album": "Blue Girl On Sunday",
        "year": "2011",
        "quantity": 8,
        "cover": "img/blue.jpg"
    },
    {
        "album": "Bleak",
        "year": "2011",
        "quantity": 0,
        "cover": "img/bleak.jpg"
    },
    {
        "album": "Wake Up, Painful Sister!",
        "year": "2012",
        "quantity": 3,
        "cover": "img/wakeup.jpg"
    },
    {
        "album": "Ministry Of Shit",
        "year": "2013",
        "quantity": 4,
        "cover": "img/ministry.jpg"
    },
    {
        "album": "Rituals",
        "year": "2013",
        "quantity": 8,
        "cover": "img/rituals.jpg"
    },
    {
        "album": "Wish I Was Here (or How Things Went Wrong) ",
        "year": "2014",
        "quantity": 2,
        "cover": "img/wish.jpg"
    },
    {
        "album": "Mlsfaw",
        "year": "2014",
        "quantity": 0,
        "cover": "img/mlsfaw.jpg"
    },
    {
        "album": "end.",
        "year": "2014",
        "quantity": 6,
        "cover": "img/end.jpg"
    },
    {
        "album": "Goretrance 9",
        "year": "2015",
        "quantity": 11,
        "cover": "img/goretrance9.jpg"
    },
    {
        "album": "With All My Heart ",
        "year": "2015",
        "quantity": 13,
        "cover": "img/myheart.jpg"
    },
    {
        "album": "Semantics: The Benzo Chronicles",
        "year": "2015",
        "quantity": 4,
        "cover": "img/semantics.jpg"
    },
    {
        "album": "I Hardly Knew You",
        "year": "2016",
        "quantity": 13,
        "cover": "img/hardly.jpg"
    },
    {
        "album": "Sedatives Vol. 1",
        "year": "2016",
        "quantity": 0,
        "cover": "img/sedatives1.jpg"
    },
    {
        "album": "Sedatives Vol. 2",
        "year": "2016",
        "quantity": 2,
        "cover": "img/sedatives2.jpg"
    },
    {
        "album": "Sedatives Vol. 3",
        "year": "2016",
        "quantity": 4,
        "cover": "img/sedatives3.jpg"
    },
    {
        "album": "You Get The Tracks You Deserve!",
        "year": "2016",
        "quantity": 11,
        "cover": "img/youget.jpg"
    },
    {
        "album": "Stankface",
        "year": "2017",
        "quantity": 15,
        "cover": "img/stankface.jpg"
    },
    {
        "album": "Goretrance X ",
        "year": "2017",
        "quantity": 19,
        "cover": "img/goretrancex.jpg"
    }
]